# sparc32

# Files
- sparc-utils_1.9.orig.tar.gz
  Contains elftoaout (needed for sparc32 netboot)

- sparc-utils_1.9-4.diff (p0)
- sparc-utils-1.9-no-implicit.patch (p1)
- elftoaout-2.3-64bit_fixes-1.patch (p0)
  Various patches to apply

- config_5.5-rc1.ultraslim
  Very slim config, no IPv6, no SCSI, pretty much nothing

# It boots
```
PROMLIB: obio_ranges 1
PROMLIB: Sun Boot Prom Version 3 Revision 2
Linux version 5.5.0-rc1 (dashie@potato) (gcc version 9.2.0 (crosstool-NG 1.24.0.37-3f461da)) #11 Thu Dec 12 09:01:07 CET 2019
printk: bootconsole [earlyprom0] enabled
ARCH: SUN4M
TYPE: SPARC CPU-5CE
Ethernet address: 00:80:42:0b:0b:5e
OF stdout device is: /obio/zs@0,100000:a
PROM: Built device tree with 33336 bytes of memory.
Booting Linux...
Power off control detected.
Built 1 zonelists, mobility grouping off.  Total pages: 7151
Kernel command line:
Dentry cache hash table entries: 4096 (order: 2, 16384 bytes, linear)
Inode-cache hash table entries: 2048 (order: 1, 8192 bytes, linear)
Sorting __ex_table...
mem auto-init: stack:off, heap alloc:off, heap free:off
Memory: 26280K/28860K available (2877K kernel code, 123K rwdata, 312K rodata, 116K init, 113K bss, 2580K reserved, 0K cma-reserved, 0K highmem)
```
